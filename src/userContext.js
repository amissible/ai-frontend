import React from "react";

const userContext = React.createContext({
  isLoggedIn: true,
  userInfo: {},
  handleUserInfo: ()=>{},
  showSigninForm: false,
  handleSignin: ()=>{},
  handleLoading: () => {},
  isSnackbarOpen: false,
  handleSnackbar: ()=>{},
  snackbarContent: {}
});

export default userContext;
