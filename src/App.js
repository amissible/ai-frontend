import { useEffect, useState } from "react";
import { BrowserRouter as Router, HashRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Dashboard from "./components/Dashboard/Dashboard";
import Home from "./components/Home/Home";
import Navbar from "./components/Navbar/Navigationbar";
import userContext from "./userContext";
import Footer from "./components/Footer/Footer";
import { Backdrop, CircularProgress, Snackbar } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";
import Signin from "./components/Signin/Signin";
import InternshipPayment from "./components/Internship/InternshipPayment";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userInfo, setUserInfo] = useState({});
  const [showSigninForm, setShowSigninForm] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);
  const [snackbarContent, setSnackbarContent] = useState({
    severity: "",
    title: "",
    text: "",
    subText: "",
  });
  const classes = useStyles();

  // useEffect(() => {
  //   setIsLoggedIn(false);
  // }, []);

  const handleUserInfo = (obj) => {
      setUserInfo(obj);
  };
  const handleLoading = (value) => {
    setIsLoading(value);
  };
  const handleSignin = (value) => {
    setShowSigninForm(value);
  }
  const handleSnackbar = (value) => {
    setIsSnackbarOpen(value);
  };
  const handleCloseSnackbar = () => {
    setIsSnackbarOpen(false);
  };
  const snackbarContentSetter = (data) => {
    setSnackbarContent(data);
  };

  return (
    <HashRouter>
      <div className="app">
        <userContext.Provider
          value={{
            isLoggedIn,
            userInfo,
            handleUserInfo,
            showSigninForm,
            handleSignin,
            isLoading,
            handleLoading,
            isSnackbarOpen,
            handleSnackbar,
            snackbarContentSetter,
          }}
        >
          <Backdrop className={classes.backdrop} open={isLoading}>
            <CircularProgress color="inherit" />
          </Backdrop>
          <Navbar />
          <Switch>
            <Route path="/dashboard" component={Dashboard} />
            {/* <Route exact path="/book-appointment" component={BookAppointment} /> */}
            <Route path="/login" component={Signin} />
            <Route path="/" component={Home} />
          </Switch>
          {/* <button onClick={()=>{console.log(isLoggedIn);}}>Print</button> */}
          <section className="footer-section" style={{zIndex: "1030"}}>
            <Footer />
          </section>
          <Snackbar
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            open={isSnackbarOpen}
            autoHideDuration={10000}
            onClose={handleCloseSnackbar}
            message="I love snacks"
            key={"signinsuccess"}
            style={{ top: "80px" }}
          >
            <Alert severity={snackbarContent.severity} className="mt-3">
              <AlertTitle>{snackbarContent.title}</AlertTitle>
              {snackbarContent.text} —{" "}
              <strong>{snackbarContent.subText}</strong>
            </Alert>
          </Snackbar>
        </userContext.Provider>
      </div>
    </HashRouter>
  );
}

export default App;
