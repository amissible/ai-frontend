import { Button, Grid, Snackbar, TextField } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import axios from "axios";
import React, { useState } from "react";
import { useContext } from "react";
import userContext from "../../userContext";
import { base_url } from "../../utils/helperData";
import "./contactus.css";

export default function ContactUs() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [subject, setSubject] = useState("");
  const [open, setOpen] = useState();

  const [msg, setMsg] = useState("");
  const [errors, setErrors] = useState({});
  const [sendSuccess, setSendSuccess] = useState(false);
  const {snackbarContentSetter, handleSnackbar} = useContext(userContext);

  const checkErrors = (value, rules) => {
    let isValid = true;
    if (rules.isRequired) {
      isValid = value.trim() !== "" && isValid;
    }
    return isValid;
  };

  const handleSubmit = async(e) => {
    e.preventDefault();
    let newErrors;
    if (!checkErrors(name, { isRequired: true }))
      newErrors = { nameError: "Please enter your name" };
    else newErrors = undefined;
    if (!checkErrors(email, { isRequired: true }))
      newErrors = { ...newErrors, emailError: "Please enter your email" };
    else newErrors = undefined;
    if (!checkErrors(subject, { isRequired: true }))
      newErrors = { ...newErrors, subjectError: "Please enter your name" };
    else newErrors = undefined;
    if (!checkErrors(msg, { isRequired: true }))
      newErrors = { ...newErrors, msgError: "Please enter your name" };
    else newErrors = undefined;
    if (!newErrors) {
      setSendSuccess(true);
      setErrors({});
      const payload = { name, email, subject, msg };
      try {
        await axios.post(`${base_url}/submit_contact`, payload);
          snackbarContentSetter({severity:'success', title:'Great!', text:'Your query has been sent successfully', subText: 'Thanks for contacting us!'});
          handleSnackbar(true);
      } catch (error) {
        snackbarContentSetter({severity:'error', title:'Failed!', text:'Your query has not been sent', subText: 'Please try again'});
          handleSnackbar(true);
      }
      
    } else {
      setErrors(newErrors);
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      {sendSuccess === true && (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          autoHideDuration={10000}
          onClose={handleClose}
          message="I love snacks"
          key={"signinsuccess"}
          style={{ top: "80px" }}
        >
          <Alert severity="success" className="mt-3">
            <AlertTitle>Great!</AlertTitle>
            Your query has been sent successfully —{" "}
            <strong>Thanks for contacting us!</strong>
          </Alert>
        </Snackbar>
      )}
      {sendSuccess === false && (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={open}
          autoHideDuration={10000}
          onClose={handleClose}
          message="I love snacks"
          key={"signinsuccess"}
          style={{ top: "80px" }}
        >
          <Alert severity="error" className="mt-3">
            <AlertTitle>Failed!</AlertTitle>
            Your query has not been sent —<strong>Please try again</strong>
          </Alert>
        </Snackbar>
        // <Alert severity="error" className="mb-4">
        //   <AlertTitle>Failed!</AlertTitle> Your query has not been sent —{" "}
        //   <strong>Please try again</strong>
        // </Alert>
      )}

      <div
        className="contact-us-div pt-4 pb-5"
        style={{ border: "1px solid white" }}
      >
        <div className="contact-us-heading text-center">
          <h1 className="display-2">CONTACT US</h1>
          {/* <h3 className="mt-3 mb-4">Who We Are?</h3> */}
        </div>

        <div className="contact-us-container">
          <Grid container justify="center">
            <Grid item xs={12} md={5}>
              {/* <Grow in={true} timeout={2000}> */}
              <div className="contact-form p-4">
                <h3 className="mt-2 mb-4 display-6">Contact Form</h3>

                <form onSubmit={handleSubmit}>
                  <div className="form-inputs d-flex flex-column">
                    <TextField
                      variant="outlined"
                      margin="dense"
                      name="name"
                      error={errors.nameError}
                      className="my-2"
                      placeholder="Enter Your Name"
                      onChange={(e) => setName(e.target.value)}
                    ></TextField>
                    <TextField
                      variant="outlined"
                      margin="dense"
                      name="email"
                      error={errors.emailError}
                      className="my-2"
                      placeholder="Enter Your Email"
                      onChange={(e) => setEmail(e.target.value)}
                    ></TextField>
                    <TextField
                      variant="outlined"
                      margin="dense"
                      name="subject"
                      error={errors.subjectError}
                      className="my-2"
                      placeholder="Enter Subject"
                      onChange={(e) => setSubject(e.target.value)}
                    ></TextField>
                    <TextField
                      variant="outlined"
                      rows={3}
                      multiline
                      className="my-2"
                      name="message"
                      error={errors.msgError}
                      placeholder="Enter Message"
                      onChange={(e) => setMsg(e.target.value)}
                    ></TextField>
                    <Button
                      variant="contained"
                      className="mt-2"
                      color="primary"
                      type="submit"
                    >
                      Send
                    </Button>
                  </div>
                </form>
              </div>
              {/* </Grow> */}
            </Grid>
            <Grid item xs={12} md={5}>
              <div className="w-100 h-100">
                {/* <img src={ContactImg} alt="" /> */}
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </>
  );
}
