import { Button } from "@material-ui/core";
import { ArrowUpward } from "@material-ui/icons";
import React, { useContext, useState } from "react";
import AboutUs from "../AboutUs/AboutUs";
import ContactUs from "../ContactUs/ContactUs";
import Services from "../Services/Services";
import SignIn from "../SigninForm/SigninForm";
import ThreeStepForm from "../ThreeStepForm/ThreeStepForm";
import "./home.css";
import { Link } from "react-scroll";
import userContext from "../../userContext";
import Wywg from "../Wywg/Wywg";

export default function Home() {
  const {showSigninForm} = useContext(userContext);

  return (
    <>
      <section className="home-section bg-cover" id="home">
        <div className="home-div d-flex justify-content-center align-items-center">
          {!showSigninForm ? (
            <ThreeStepForm />
          ) : (
            <SignIn />
          )}
        </div>
      </section>
      <section className="about-us-section my-5" id="about">
        <AboutUs />
      </section>
      <section className="wywg">
        <Wywg />
      </section>
      <section className="services-section my-5 pt-3" id="services">
        <Services />
      </section>
      <section className="contact-us-section mt-5" id="contact-us">
        <ContactUs />
      </section>
      <Link
        to="home"
        spy={true}
        smooth={true}
        duration={1000}
        className="back-to-top"
      >
        <Button variant="contained" color="primary" className="py-2">
          <ArrowUpward />
        </Button>
      </Link>
    </>
  );
}
