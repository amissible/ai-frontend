import { Grid } from "@material-ui/core";
import React from "react";
import "./internshippayment.css";
import { TextField, MenuItem } from "@material-ui/core";
import InternshipImage from "../../assets/internship.jpg";
import { useState } from "react";
import { Button } from "@material-ui/core";
import PaytmLogo from "../../assets/paytm.png";
import UpiLogo from "../../assets/upi.png";

export default function InternshipPayment() {
  const [months, setMonths] = useState("");
  return (
    <section className="internship-payment-section mb-5">
      <div className="internship-payment-div">
        <Grid container justify="center">
          <Grid container xs={12}>
            <Grid item xs={6} className="internship-left-div">
              <img src={InternshipImage} alt="" className="main-image" />
            </Grid>
            <Grid item xs={6} className="internship-right-div py-5">
              <h3 className="display-6 pt-4 pb-5 text-center">
                Start Your Internship
              </h3>
              <form className="internship-payment-form d-flex align-items-center flex-column">
                <TextField select className="w-75 mb-2" label="Select Batch*">
                  <MenuItem value="1-july">1-July</MenuItem>
                </TextField>

                <TextField
                  className="w-75 my-2"
                  select
                  name="months"
                  label="Select Months*"
                  value={months}
                  onChange={(e) => setMonths(e.target.value)}
                >
                  {["1", "6"].map((num) => (
                    <MenuItem key={num} value={num}>
                      {num} {num === "1" ? "Month" : "Months"}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  disabled
                  value={
                    months !== ""
                      ? months === "1"
                        ? "Rs.3000"
                        : "Rs.15000"
                      : "Rs.0"
                  }
                  className="w-75 my-4"
                ></TextField>
                <Button
                  variant="contained"
                  color="primary"
                  className="my-3 w-75"
                >
                  Pay Now
                </Button>
                <div className="payment-options mt-4">
                  Payment Options-{" "}
                  <span className="mx-1">
                    <img src={PaytmLogo} alt="" width="60" />
                  </span>
                  <span className="mx-1">
                    <img src={UpiLogo} alt="" width="60" />
                  </span>
                </div>
              </form>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </section>
  );
}
