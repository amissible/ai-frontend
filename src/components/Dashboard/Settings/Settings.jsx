import React from 'react'
import './settings.css'
import { Grid } from "@material-ui/core";
import Img from '../../../assets/bug.png'

export default function Settings() {
    return (
        <div className="settings-div mb-5">
            <Grid container className="p-2">
                <Grid item xs={12} md={6}>
                    <h2 className="display-6 px-3 py-3">Account Settings</h2>
                    <div className="settings d-flex justify-content-center">
                        <p className="">Change your account settings</p>
                    </div>
                </Grid>
                <Grid item xs={12} md={6}><img src={Img} alt="" className="w-100" /></Grid>
            </Grid>
        </div>
    )
}
