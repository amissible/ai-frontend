import React from "react";
import "react-calendar/dist/Calendar.css";
import "./dashboard.css";
import { base_url } from "../../utils/helperData";
import CssBaseline from "@material-ui/core/CssBaseline";

import { Route, Switch } from "react-router-dom";
import Sidebar from "./Sidebar";
import { useState } from "react";
import BookAppointment from "./BookAppointment/BookAppointment";
import { makeStyles } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { Grid } from "@material-ui/core";
import Image from "../../assets/personal-website.png";
import {
  AssignmentTurnedInOutlined,
  CalendarTodayOutlined,
  ComputerOutlined,
  EmailOutlined,
  PersonOutline,
  WcOutlined,
} from "@material-ui/icons";
import ViewAppointment from "./ViewAppointment/ViewAppointment";
import Payments from "./Payments/Payments";
import Settings from "./Settings/Settings";
import { useEffect, useContext } from "react";
import axios from "axios";
import userContext from "../../userContext";
import InternshipPayment from "../Internship/InternshipPayment";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },

  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
  },
}));

export default function Dashboard() {
  const [userInfo, setUserInfo] = useState([]);
  const classes = useStyles();
  const { handleLoading } = useContext(userContext);

  useEffect(() => {
    handleLoading(true);
    const email = localStorage.getItem("email");
    axios
      .get(`${base_url}/quick_evaluation/GetByEmail?email=${email}`)
      .then((res) => {
        const values = [
          {
            icon: <PersonOutline />,
            title: "Name:",
            value: `${res.data.firstName} ${res.data.lastName}`,
          },
          {
            icon: <AssignmentTurnedInOutlined />,
            title: "Qualification:",
            value: res.data.qualification,
          },
          {
            icon: <EmailOutlined />,
            title: "Email:",
            value: res.data.email,
          },
          { icon: <WcOutlined />, title: "Gender:", value: "Male" },
          {
            icon: <CalendarTodayOutlined />,
            title: "Date Of Birth:",
            value: res.data.dob,
          },
          {
            icon: <ComputerOutlined />,
            title: "Skills:",
            value: res.data.skills,
          },
        ];
        setUserInfo(values);
        handleLoading(false);
      })
      .catch((error) => {
        console.log(error);
        handleLoading(false);
      });
  }, []);

  return (
    <section className="dashboard-section">
      <div className="dashboard-div">
        <div className={classes.root}>
          <CssBaseline />
          <Sidebar />
          <main
            className={classes.content}
            style={{ paddingLeft: "20px", paddingRight: "20px" }}
          >
            {/* <div className={classes.toolbar} /> */}

            <Switch>
              <Route
                path="/dashboard/view-appointments"
                component={ViewAppointment}
              />
              <Route
                path="/dashboard/book-appointment"
                component={BookAppointment}
              />
              <Route
                path="/dashboard/internship"
                component={InternshipPayment}
              />
              <Route path="/dashboard/payments" component={Payments} />
              <Route path="/dashboard/account-settings" component={Settings} />
              <Route path="/dashboard">
                <div className="dashboard-content py-3 px-3 mb-5">
                  <Grid container>
                    <Grid item xs={6}>
                      <h2 className="display-6 pt-2 pb-4">Dashboard</h2>
                      {userInfo.map((item) => (
                        <Paper className="p-3 my-2">
                          <Grid
                            container
                            justify="space-between"
                            className="align-items-bottom"
                          >
                            <div className="item-left">
                              {item.icon} &nbsp; {item.title}
                            </div>
                            <div className="item-right">{item.value}</div>
                          </Grid>
                        </Paper>
                      ))}
                    </Grid>
                    <Grid
                      item
                      xs={6}
                      alignItems="center"
                      className="dashboard-img p-3"
                    >
                      <Grid container style={{ width: "100%", height: "100%" }}>
                        <img src={Image} alt="" />
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </Route>
            </Switch>
          </main>
        </div>
      </div>
    </section>
  );
}
