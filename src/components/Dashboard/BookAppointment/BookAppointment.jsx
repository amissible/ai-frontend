// import { Grid, MenuItem, TextField } from "@material-ui/core";
// import React, { useState } from "react";
// import Calendar from "react-calendar";
// import { makeStyles } from "@material-ui/core/styles";
// import Stepper from "@material-ui/core/Stepper";
// import Step from "@material-ui/core/Step";
// import StepLabel from "@material-ui/core/StepLabel";
// import StepContent from "@material-ui/core/StepContent";
// import Button from "@material-ui/core/Button";
// import Typography from "@material-ui/core/Typography";
// import PaytmLogo from "../../../assets/paytm.png";
// import UpiLogo from "../../../assets/upi.png";

// import "./bookappointment.css";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     width: "100%",
//   },
//   button: {
//     marginTop: theme.spacing(1),
//     marginRight: theme.spacing(1),
//   },
//   actionsContainer: {
//     marginBottom: theme.spacing(2),
//   },
//   resetContainer: {
//     padding: theme.spacing(3),
//   },
// }));

// function getSteps() {
//   return ["Appointment Date", "Payment", "Finalize"];
// }

// function getStepContent(step) {
//   switch (step) {
//     case 0:
//       return `Select the date for your appointment`;
//     case 1:
//       return "Pay the appointment fee";
//     case 2:
//       return `Review appoinment details`;
//     default:
//       return "Unknown step";
//   }
// }

// export default function BookAppointment() {
//   const [value, setValue] = useState(new Date());
//   const [months, setMonths] = useState("");
//   const [isPaid, setIsPaid] = useState(false);
//   const [amount, setAmount] = useState(0);

//   const classes = useStyles();
//   const [activeStep, setActiveStep] = useState(0);
//   const steps = getSteps();

//   const handleNext = () => {
//     setActiveStep((prevActiveStep) => prevActiveStep + 1);
//   };

//   const handleBack = () => {
//     setActiveStep((prevActiveStep) => prevActiveStep - 1);
//   };

//   //   const handleReset = () => {
//   //     setActiveStep(0);
//   //   };

//   return (
//     <section className="book-appointment">
//       <div className="book-appointment-div px-4 pb-5">
//         <Grid container justify="center">
//           <Grid item xs={12}>
//             <div className="appointment-header"></div>
//             <div className="appointment-body mb-3">
//               <Grid container justify="center" className="mt-5">
//                 <Grid item xs={5}>
//                   <Grid container>
//                     <h2 className="display-6">Book Appointment</h2>
//                     <Stepper
//                       activeStep={activeStep}
//                       orientation="vertical"
//                       style={{ background: "transparent" }}
//                     >
//                       {steps.map((label, index) => (
//                         <Step key={label}>
//                           <StepLabel>{label}</StepLabel>
//                           <StepContent>
//                             <Typography>{getStepContent(index)}</Typography>
//                             <div className={classes.actionsContainer}>
//                               <div>
//                                 <Button
//                                   disabled={activeStep === 0}
//                                   onClick={handleBack}
//                                   className={classes.button}
//                                 >
//                                   Back
//                                 </Button>
//                                 <Button
//                                   variant="contained"
//                                   color="primary"
//                                   disabled={
//                                     activeStep === 1 && !isPaid ? true : false
//                                   }
//                                   onClick={handleNext}
//                                   className={classes.button}
//                                 >
//                                   {activeStep === steps.length - 1
//                                     ? "Finish"
//                                     : "Next"}
//                                 </Button>
//                               </div>
//                             </div>
//                           </StepContent>
//                         </Step>
//                       ))}
//                     </Stepper>
//                   </Grid>
//                 </Grid>
//                 <Grid item xs={7}>
//                   <Grid container justify="center">
//                     {activeStep === 0 && (
//                       <>
//                         <Grid item>
//                           <h2 className="mb-4">Select date for appointment</h2>

//                           <Grid container justify="center">
//                             <Calendar
//                               onChange={setValue}
//                               minDate={new Date()}
//                               value={value}
//                               style={{ background: "transparent" }}
//                             />
//                           </Grid>
//                         </Grid>
//                       </>
//                     )}
//                     {activeStep === 1 && (
//                       <>
//                         <Grid item>
//                           <div className="payment">
//                             <h2>Payment</h2>

//                             <label htmlFor="amount" className="mt-4">
//                               Appointment Fee
//                             </label>

//                             <TextField
//                               className="mb-4"
//                               name="amount"
//                               disabled
//                               fullWidth
//                               margin="none"
//                               value={200}
//                             ></TextField>

//                             <br />
//                             <Button
//                               fullWidth
//                               variant="contained"
//                               color="primary"
//                               className="mb-4"
//                             >
//                               Pay Now
//                             </Button>
//                             <div className="payment-options">
//                               Payment Options-{" "}
//                               <span className="mx-1">
//                                 <img src={PaytmLogo} alt="" width="60" />
//                               </span>
//                               <span className="mx-1">
//                                 <img src={UpiLogo} alt="" width="60" />
//                               </span>
//                             </div>
//                           </div>
//                         </Grid>
//                       </>
//                     )}

//                     {activeStep === 2 && (
//                       <Grid item>
//                         <div className="review-appointment">
//                           <h3>Review Your Appoinment</h3>
//                           <p>Name: </p>
//                           <p>
//                             Your appointment date is:{" "}
//                             {`${value.getDate()}/${value.getMonth()}/${value.getFullYear()}`}
//                           </p>
//                           <p>Appointment Fee status: </p>
//                           <p>Appointment Fee amount: </p>
//                           <Button variant="contained" color="secondary">
//                             Book Appointment
//                           </Button>
//                         </div>
//                       </Grid>
//                     )}
//                   </Grid>
//                 </Grid>
//               </Grid>
//             </div>
//           </Grid>
//         </Grid>
//       </div>
//     </section>
//   );
// }

import { Grid, MenuItem, TextField } from "@material-ui/core";
import React, { useState } from "react";
import Calendar from "react-calendar";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import PaytmLogo from "../../../assets/paytm.png";
import UpiLogo from "../../../assets/upi.png";

import "./bookappointment.css";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

function getSteps() {
  return ["Appointment Date", "Payment", "Finalize"];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return `Select the date for your appointment`;
    case 1:
      return "Pay the appointment fee";
    case 2:
      return `Review appoinment details`;
    default:
      return "Unknown step";
  }
}

export default function BookAppointment() {
  const [value, setValue] = useState();
  // const [value, setValue] = useState(new Date());
  const [months, setMonths] = useState("");
  const [isPaid, setIsPaid] = useState(false);
  const [amount, setAmount] = useState(0);

  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  //   const handleReset = () => {
  //     setActiveStep(0);
  //   };

  return (
    <section className="book-appointment">
      <div className="book-appointment-div">
        <Grid container justify="center" className="px-4 py-5">
          <Grid item xs={6}>
            <h2 className="display-6">Book Appointment</h2>
            <Stepper
              activeStep={activeStep}
              orientation="vertical"
              style={{ background: "transparent" }}
            >
              {steps.map((label, index) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                  <StepContent>
                    <Typography>{getStepContent(index)}</Typography>
                    <div className={classes.actionsContainer}>
                      <div>
                        <Button
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          className={classes.button}
                        >
                          Back
                        </Button>
                        <Button
                          variant="contained"
                          color="primary"
                          disabled={activeStep === 1 && !isPaid ? true : false}
                          onClick={handleNext}
                          className={classes.button}
                        >
                          {activeStep === steps.length - 1 ? "Finish" : "Next"}
                        </Button>
                      </div>
                    </div>
                  </StepContent>
                </Step>
              ))}
            </Stepper>
          </Grid>

          <Grid item xs={6} className="p-5">
            {activeStep === 0 && (
              <>
                <Grid item>
                  <h2 className="mb-4">Select date for appointment</h2>

                  <Calendar
                    onChange={setValue}
                    minDate={new Date().setDate()}
                    value={value}
                    style={{ background: "transparent" }}
                    tileDisabled={({date})=>{return (date.getDay()===0 || date.getDay() ===6)}}
                  />
                </Grid>
              </>
            )}
            {activeStep === 1 && (
              <>
                <Grid item>
                  <div className="payment">
                    <h2>Payment</h2>

                    <label htmlFor="amount" className="mt-4">
                      Appointment Fee
                    </label>

                    <TextField
                      className="mb-4"
                      name="amount"
                      disabled
                      fullWidth
                      margin="none"
                      value={200}
                    ></TextField>

                    <br />
                    <Button
                      fullWidth
                      variant="contained"
                      color="primary"
                      className="mb-4"
                    >
                      Pay Now
                    </Button>
                    <div className="payment-options">
                      Payment Options-{" "}
                      <span className="mx-1">
                        <img src={PaytmLogo} alt="" width="60" />
                      </span>
                      <span className="mx-1">
                        <img src={UpiLogo} alt="" width="60" />
                      </span>
                    </div>
                  </div>
                </Grid>
              </>
            )}

            {activeStep === 2 && (
              <Grid item>
                <div className="review-appointment">
                  <h3>Review Your Appoinment</h3>
                  <p>Name: </p>
                  <p>
                    Your appointment date is:{" "}
                    {`${value.getDate()}/${value.getMonth()}/${value.getFullYear()}`}
                  </p>
                  <p>Appointment Fee status: </p>
                  <p>Appointment Fee amount: </p>
                  <Button variant="contained" color="secondary">
                    Book Appointment
                  </Button>
                </div>
              </Grid>
            )}
          </Grid>
        </Grid>
      </div>
    </section>
  );
}
