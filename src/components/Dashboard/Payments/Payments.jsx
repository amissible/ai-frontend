import React from 'react'
import './payments.css'
import Img from '../../../assets/apple-pay.png'
import { Grid } from "@material-ui/core";


export default function Payments() {
    return (
        <div className="payments-div mb-5">
            <Grid container className="p-2">
                <Grid item xs={12} md={6}>
                    <h2 className="display-6 px-3 py-3">Payments</h2>
                    <div className="payments d-flex justify-content-center">
                        {/* <p className="">No Appointments Booked</p> */}
                    </div>
                </Grid>
                <Grid item xs={12} md={6}><img src={Img} alt="" className="w-100" /></Grid>
            </Grid>
        </div>
    )
}
