import { Grid } from "@material-ui/core";
import React from "react";
import "./viewappointment.css";
import Img from "../../../assets/booking-booked.png";

export default function ViewAppointment() {
  return (
    <div className="view-appointment-div">
      <Grid container className="p-2">
        <Grid item xs={12} md={6}>
          <h2 className="display-6 px-3 py-3">View Your Appointments</h2>
          <div className="appointments d-flex justify-content-center">
            <p className="">No Appointments Booked</p>
          </div>
        </Grid>
        <Grid item xs={12} md={6}>
          <img src={Img} alt="" className="w-100" />
        </Grid>
      </Grid>
    </div>
  );
}
