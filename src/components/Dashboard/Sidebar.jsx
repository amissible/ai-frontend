import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { useTheme } from "@material-ui/core/styles";
import {
  Link,
  useRouteMatch,
} from "react-router-dom";
import {
  AddBoxRounded,
  DashboardOutlined,
  ExitToApp,
  Payment,
  SettingsApplications,
  Visibility,
  AssignmentInd
} from "@material-ui/icons";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },

  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function Sidebar(props) {
  const { window } = props;

  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  let match = useRouteMatch();

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider className="mb-5" />
      <List>
        {[
          { title: "Dashboard", route: "/", icon: <DashboardOutlined /> },
          {
            title: "Book Appointment",
            route: "/book-appointment",
            icon: <AddBoxRounded />,
          },
          {
            title: "View Appointment",
            route: "/view-appointments",
            icon: <Visibility />,
          },
          {
            title: "Internship",
            route: "/internship",
            icon: <AssignmentInd />,
          },
          { title: "Payments", route: "/payments", icon: <Payment /> },
          {
            title: "Account Settings",
            route: "/account-settings",
            icon: <SettingsApplications />,
          },
          { title: "Logout", route: "/logout", icon: <ExitToApp /> },
        ].map(({ title, icon, route }) => (
          <Link to={`/dashboard${route}`} style={{textDecoration: "none", color: "rgb(100,100,100)"}}>
            <ListItem button key={title}>
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText primary={title} />
            </ListItem>
          </Link>
        ))}
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <nav
      className={classes.drawer}
      aria-label="mailbox folders"
      style={{ zIndex: "1" }}
    >
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smUp implementation="css">
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {drawer}
        </Drawer>
      </Hidden>
    </nav>
  );
}
