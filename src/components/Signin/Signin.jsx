import { Grid } from "@material-ui/core";
import React from "react";
import SignInForm from "../SigninForm/SigninForm";
import "./signin.css";

export default function Signin() {
  return (
    <section className="signin-section">
      <div className="signin-div">
          <Grid container justify="center">
              <Grid item xs={5}>
              <SignInForm />
              </Grid>
          </Grid>
      </div>
    </section>
  );
}


