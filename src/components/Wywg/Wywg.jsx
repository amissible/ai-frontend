import { Grid } from "@material-ui/core";
import React from "react";
import "./wywg.css";
import Img1 from "../../assets/javascript-coding-language.png";
import Img2 from "../../assets/support-team.png";
import Img3 from "../../assets/certificate.svg";
import Img4 from "../../assets/client-meeting.png";
import Img5 from "../../assets/team-meeting.png";

export default function Wywg() {
  return (
    <div className="gains-div">
        <h1 className="text-center display-4">WHAT YOU GET</h1>
      <Grid container justify="center" className="my-5">
        <Grid item xs={12} md={5} className="p-4 d-flex align-items-center">
          {/* <Paper elevation={12} className="p-4"> */}
          <div className="gains-item">
            <h3 className="text-center pb-2">
              Near Industry Training <br /> & <br /> Work Experience
            </h3>
            <p style={{textIndent: "100px"}}>
            Through Internsible, we intend to offer the candidates familiarisation, training and adaptation of current software industry infrastructural tools and techniques. At the same time, they get real industry exposure while working with a team of domain experts and leaders on in-house/ client projects. Here they get an opportunity to transform their theoretical learnings into practical insight, paving the way for their potential evolution as software professionals in near future.
            </p>
          </div>
          {/* </Paper> */}
        </Grid>
        <Grid item xs={12} md={5} className="d-flex justify-content-center">
          <img src={Img1} alt="" className="w-75" />
        </Grid>
      </Grid>
      <Grid container justify="center" className="my-5">
        <Grid item xs={12} md={5} className="d-flex justify-content-center">
          <img src={Img2} alt="" className="w-75" />
        </Grid>
        <Grid item xs={12} md={5} className="p-4 d-flex align-items-center">
          {/* <Paper elevation={12} className="p-4"> */}
          <div className="gains-item">
            <h3 className="text-center pb-2">Potential Career Oppurtunities</h3>
            <p style={{textIndent: "100px"}}>
            After successful completion of the internship, based on performance, skill set and availability of matching position, select candidates may get job opportunities with client firm in the following fields. This may turn out to be a stepping stone to loftier ambitions to be professional.
            </p>
          </div>
          {/* </Paper> */}
        </Grid>
      </Grid>
      <Grid container justify="center" className="my-5">
        <Grid item xs={12} md={5} className="p-4 d-flex align-items-center">
          {/* <Paper elevation={12} className="p-4"> */}
          <div className="gains-item">
            <h3 className="text-center pb-2">Certificate</h3>
            <p style={{textIndent: "100px"}}>
            We intend to provide verifiable Certificate listing skills learnt during the internship program. It is supposed to act as a documented proof of knowledge enhancement by providing individuals with excellent internship opportunities and mentoring budding talents and developing them into successful professionals of tomorrow.
            </p>
          </div>
          {/* </Paper> */}
        </Grid>
        <Grid item xs={12} md={5} className="d-flex justify-content-center">
          <img src={Img3} alt="" className="w-50" />
        </Grid>
      </Grid>
      <Grid container justify="center" className="my-5">
        <Grid item xs={12} md={5} className="d-flex justify-content-center">
          <img src={Img4} alt="" className="w-100" />
        </Grid>
        <Grid item xs={12} md={5} className="p-4 d-flex align-items-center">
          {/* <Paper elevation={12} className="p-4"> */}
          <div className="gains-item">
            <h3 className="text-center pb-2">Communication Skills</h3>
            <p style={{textIndent: "100px"}}>
            Candidates get opportunity to know the software industry jorgans, their frequent uses based on utility through the participation in the two-way process of communication during entire program. This eliminates hesitation and further supports better expression of ideas through enriched corpus of expressing tools and higher sense of application, matching current trends.
            </p>
          </div>
          {/* </Paper> */}
        </Grid>
      </Grid>
      <Grid container justify="center" className="my-5">
        <Grid item xs={12} md={5} className="p-4 d-flex align-items-center">
          {/* <Paper elevation={12} className="p-4"> */}
          <div className="gains-item">
            <h3 className="text-center pb-2">Team Strategy</h3>
            <p style={{textIndent: "100px"}}>
            The adoption of Scrum model of deliverables throughout the program inculcates team spirit among candidates. It further provides candidates opportunities to discuss the problem across the domain, brainstorm collectively. This enhances problem solving skills considerably through holistic viewing, analysis and resolution of challenges.
            </p>
          </div>
          {/* </Paper> */}
        </Grid>
        <Grid item xs={12} md={5} className="d-flex justify-content-center">
          <img src={Img5} alt="" className="w-75" />
        </Grid>
      </Grid>
    </div>
  );
}
