import React from "react";
import './footer.css';

export default function Footer() {
  return (
    <div className="footer-div">
      <h5
        className="text-center py-4 text-white m-0"
      >
        Copyright &copy; 2021 Internsible
      </h5>
    </div>
  );
}
