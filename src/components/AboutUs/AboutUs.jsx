import { Grid, Paper } from "@material-ui/core";
import React from "react";
import "./aboutus.css";

export default function AboutUs() {
  return (
    <div className="about-us-div" id="about-us">
      <div className="about-us-heading text-center mt-5 mb-5">
        <h1 className="display-2">ABOUT US</h1>
        <h3 className="mt-3 mb-4">Who We Are?</h3>
      </div>
      <div className="about-us-container">
        <Grid container justify="space-around">
          <Grid item xs={12} md={5}>
            {/* <Grow in={true} timeout={2000}> */}
              <Paper square elevation={12} className="p-4 text-justify">
                <h2 className="display-5 mb-4">Our Vision</h2>
                <p className="text-content">
                  Internsible is a platform where students should have the
                  opportunity to create progress through technology and develop
                  the skills of tomorrow. They find meaningful internships.
                </p>
                <p className="text-content">
                  We are a technology company on a mission to equip students
                  with relevant skills & practical exposure through internships
                  and online pieces of training. Imagine a world full of freedom
                  and possibilities.
                </p>
              </Paper>
            {/* </Grow> */}
          </Grid>
          <Grid item xs={12} md={5}>
            <Paper square elevation={12} className="p-4 text-justify">
              <h2 className="display-5 mb-4">Our Mission</h2>
              <p>We lift leaders who lift the world.</p>
              <p>Level up with practical life skills.</p>
              <p className="text-content">
               We empower and inspire our learners and ourselves to
                develop through the power of lifelong learning.
              </p>
            </Paper>
          </Grid>
        </Grid>
        <Grid container justify="center" className="mt-5">
          <Grid item xs={11}>
            <Paper square elevation={12} className="p-4 text-justify">
              <h2 className="display-5 mb-4">What We Believe In</h2>
              <p className="text-content">
                We like to change things. We believe in human potential. We
                believe in opportunities. Letsintern is a culmination of these
                believes.
              </p>
              <p className="text-content">
                We are focused on building a future where students can
                experience real opportunities while they continue their
                education and build a smarter future.
              </p>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
