import React, { useContext, useState } from "react";
import { Alert, AlertTitle } from "@material-ui/lab";
import {
  TextField,
  MenuItem,
  Button,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import "./threestepform.css";
import {
  branches,
  courses,
  areasOfInterest,
  skillsList,
  base_url,
} from "../../utils/helperData";
import ReCAPTCHA from "react-google-recaptcha";
import { Autocomplete } from "@material-ui/lab";
import axios from "axios";
import userContext from "../../userContext";

export default function ThreeStepForm() {
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [dob, setDob] = useState("");
  const [course, setCourse] = useState("");
  const [branch, setBranch] = useState("");
  const [skills, setSkills] = useState([]);
  const [aoi, setAoi] = useState("");
  const [haveComputer, setHaveComputer] = useState(false);
  const [haveInternet, setHaveInternet] = useState(false);
  const [preferredMonths, setPreferredMonths] = useState("");
  const [errors, setErrors] = useState({});
  const [step, setStep] = useState(1);
  const [captchaVerified, setCaptchaVerified] = useState(false);
  const [sendSuccess, setSendSuccess] = useState("");
  const { handleLoading, handleSnackbar, snackbarContentSetter, handleSignin,userInfo, handleUserInfo } = useContext(userContext);

  const handleSubmit = async (e) => {
    e.preventDefault();
    let newErrors;
    if (!checkErrors(fname, { isRequired: true }))
      newErrors = { fnameError: "Please enter your first name" };
    else newErrors = undefined;

    if (!checkErrors(lname, { isRequired: true }))
      newErrors = { ...newErrors, lnameError: "Please enter your last name" };
    else newErrors = undefined;

    if (!checkErrors(email, { isRequired: true }))
      newErrors = {
        ...newErrors,
        emailError: "Please enter your email address",
      };
    else newErrors = undefined;

    if (!checkErrors(gender, { isRequired: true }))
      newErrors = { ...newErrors, genderError: "Please select your gender" };
    else newErrors = undefined;

    if (!checkErrors(dob, { isRequired: true }))
      newErrors = {
        ...newErrors,
        dobError: "Please select your date of birth",
      };
    else newErrors = undefined;

    if (!captchaVerified) {
      newErrors = {
        ...newErrors,
        captchaVerifiedError: "Please verify the captcha",
      };
    }

    if (!newErrors) {
      setErrors({});
      handleLoading(true);
      localStorage.setItem("email", email);

      const skillsString = skills.toString();
      setSendSuccess(true);
      const payload = {
        firstName: fname,
        lastName: lname,
        email: email,
        dob: dob,
        gender: gender,
        qualification: course,
        branch: branch,
        areaOfInterest: aoi,
        computerAccess: haveComputer,
        internetAccess: haveInternet,
        // preferredMonths,
        skills: skillsString,
      };
      try {
        const response = await axios.post(
          `${base_url}/quick_evaluation/submit`,
          payload,
          { body: payload }
        );
        console.log(response);
        handleUserInfo({...userInfo,email});
        handleLoading(false);
        handleSignin(true);
        snackbarContentSetter({severity:'success', title:'Great!', text:'You are registered successfully', subText: 'Check your Email for OTP!'});
        handleSnackbar(true);
      } catch (error) {
        snackbarContentSetter({severity:'error', title:'Error!', text:'Some error occured', subText: 'Please try again!'});
        console.log(error);
        handleSnackbar(true);
        handleLoading(false);
      }
    } else {
      setErrors(newErrors);
    }
  };

  const checkErrors = (value, rules) => {
    let isValid = true;
    if (rules.isRequired) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.arrayLength) {
      isValid = value.length < 1 && isValid;
    }
    return isValid;
  };

  const checkAndValidate = (e) => {
    const key = e.target.name;
    const value = e.target.value;

    if (key === "course") {
      setCourse(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, courseError: true });
      else setErrors({ ...errors, courseError: false });
    } else if (key === "aoi") {
      setAoi(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, aoiError: true });
      else setErrors({ ...errors, aoiError: false });
    } else if (key === "skills") {
    } else if (key === "prefmonths") {
      setPreferredMonths(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, preferredMonthsError: true });
      else setErrors({ ...errors, preferredMonthsError: false });
    } else if (key === "skills") {
      setSkills(value);
    } else if (key === "fname") {
      setFname(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, fnameError: true });
      else setErrors({ ...errors, fnameError: false });
    } else if (key === "lname") {
      setLname(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, lnameError: true });
      else setErrors({ ...errors, lnameError: false });
    } else if (key === "email") {
      setEmail(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, emailError: true });
      else setErrors({ ...errors, emailError: false });
    } else if (key === "gender") {
      setGender(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, genderError: true });
      else setErrors({ ...errors, genderError: false });
    } else if (key === "dob") {
      setDob(value);
      if (!checkErrors(value, { isRequired: true }))
        setErrors({ ...errors, dobError: true });
      else setErrors({ ...errors, dobError: false });
    }
  };

  const handleStepOne = () => {
    let newErrors;
    if (!checkErrors(course, { isRequired: true }))
      newErrors = { courseError: "Please select your qualification" };
    else newErrors = undefined;

    if (!checkErrors(aoi, { isRequired: true }))
      newErrors = {
        ...newErrors,
        aoiError: "Please select your area of interest",
      };
    else newErrors = undefined;

    if (!newErrors) {
      setErrors({});
      setStep(2);
    } else {
      setErrors(newErrors);
    }
  };
  const handleStepTwo = () => {
    let newErrors;
    if (!checkErrors(preferredMonths, { isRequired: true }))
      newErrors = { preferredMonthsError: "Please select your qualification" };
    else newErrors = undefined;

    if(skills.length > 5)
      newErrors = {...newErrors, skillsError: "Select maximum of 5 skills"};
    else newErrors = undefined;

    if (!newErrors) {
      setErrors({});
      setStep(3);
    } else {
      setErrors(newErrors);
    }
  };

  const onCaptchaChange = (value) => {
    console.log("Captcha value: ", value);
    setCaptchaVerified(true);
  };

  return (
    <>
      <div className="three-step-form">
        <div className="form-head d-flex flex-column align-items-center">
          <h2 className="">GETTING STARTED</h2>
          <hr className="w-75" />
          {sendSuccess === false && (
            <Alert severity="error" className="">
              <AlertTitle>Failed!</AlertTitle> Your query has not been sent —{" "}
              <strong>Please try again</strong>
            </Alert>
          )}
        </div>
        <div className="form-body px-4 pb-3">
          <form onSubmit={handleSubmit} className="pt-2">
            {step === 1 && (
              <div className="step-one">
                <TextField
                  className="w-100 my-2"
                  select
                  error={errors.courseError}
                  margin="dense"
                  helperText={errors.courseError}
                  name="course"
                  id="qualification"
                  label="Qualification *"
                  placeholder="Qualification"
                  value={course}
                  onChange={checkAndValidate}
                >
                  {courses.map((course) => (
                    <MenuItem key={course} value={course}>
                      {course}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  className="w-100 my-2"
                  select
                  disabled={course === "BE/B.Tech- Bachelor of Technology" ? false : true}
                  name="branch"
                  id="branch"
                  label="Branch"
                  value={branch}
                  onChange={(e) => setBranch(e.target.value)}
                >
                  {branches.map((branch) => (
                    <MenuItem key={branch} value={branch}>
                      {branch}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  className="w-100 my-2"
                  select
                  error={errors.aoiError}
                  helperText={errors.aoiError}
                  name="aoi"
                  id="area-of-interest"
                  label="Area Of Interest *"
                  value={aoi}
                  onChange={checkAndValidate}
                >
                  {areasOfInterest.map((aoi) => (
                    <MenuItem key={aoi} value={aoi}>
                      {aoi}
                    </MenuItem>
                  ))}
                </TextField>
                <Button
                  variant="contained"
                  color="primary"
                  className="mt-5 text-center w-100"
                  onClick={handleStepOne}
                  id="stepOne-next"
                >
                  Next
                </Button>
              </div>
            )}
            {step === 2 && (
              <div className="step-two">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={haveComputer}
                      onChange={() => setHaveComputer(!haveComputer)}
                      name="haveComputer"
                      color="primary"
                      id="have-computer"
                    />
                  }
                  label="Do you have access to a computer at home?"
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      checked={haveInternet}
                      onChange={() => setHaveInternet(!haveInternet)}
                      name="haveInternet"
                      color="primary"
                      id="have-internet"
                    />
                  }
                  label="Do you have an active Internet connection?"
                />

                <TextField
                  className="w-100 my-2"
                  select
                  error={errors.preferredMonthsError}
                  helperText={errors.preferredMonthsError}
                  name="prefmonths"
                  id="preferred-months"
                  label="Preferred months for Internship *"
                  value={preferredMonths}
                  onChange={checkAndValidate}
                >
                  {["1","3","6"].map((num) => (
                    <MenuItem key={num} value={num}>
                      {num} {num === "1" ? "Month" : "Months"}
                    </MenuItem>
                  ))}
                </TextField>

                <Autocomplete
                  multiple
                  options={skillsList}
                  error={errors.skillsError}
                  helperText={errors.skillsError}
                  disableCloseOnSelect
                  value={skills}
                  autoHighlight
                  id="skills"
                  onChange={(e, values) => setSkills(values)}
                  getOptionLabel={(option) => option}
                  renderInput={(props) => (
                    <TextField
                      {...props}
                      label="Add Skills"
                      className="my-2 w-100"
                    />
                  )}
                />
                {errors.skillsError && <p className="alert alert-danger py-1">{errors.skillsError}</p>}

                <Button
                  variant="contained"
                  color="secondary"
                  className="w-50 mx-2 mt-4"
                  onClick={() => setStep(1)}
                  id="stepTwo-prev"
                >
                  Previous
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  className="w-25 mt-4"
                  onClick={handleStepTwo}
                  id="stepTwo-next"
                >
                  Next
                </Button>
              </div>
            )}

            {step === 3 && (
              <div className="step-three">
                <div className="d-flex justify-content-between">
                  <TextField
                    className="w-45 my-2"
                    label="First Name *"
                    margin="dense"
                    error={errors.fnameError}
                    helperText={errors.fnameError}
                    value={fname}
                    name="fname"
                    id="first-name"
                    onChange={checkAndValidate}
                  ></TextField>
                  <TextField
                    className="w-45 my-2"
                    label="Last Name *"
                    margin="dense"
                    error={errors.lnameError}
                    helperText={errors.lnameError}
                    value={lname}
                    name="lname"
                    id="last-name"
                    onChange={checkAndValidate}
                  ></TextField>
                </div>
                <TextField
                  className="w-100 my-2"
                  label="Email *"
                  margin="dense"
                  error={errors.emailError}
                  helperText={errors.emailError}
                  value={email}
                  name="email"
                  id="register-email"
                  type="email"
                  onChange={checkAndValidate}
                ></TextField>
                <TextField
                  className="w-100 my-2"
                  label="Gender *"
                  name="gender"
                  margin="dense"
                  id="gender"
                  value={gender}
                  select
                  error={errors.genderError}
                  helperText={errors.genderError}
                  onChange={checkAndValidate}
                >
                  <MenuItem value="Male">Male</MenuItem>
                  <MenuItem value="Female">Female</MenuItem>
                  <MenuItem value="Other">Other</MenuItem>
                </TextField>
                <TextField
                  label="Date Of Birth *"
                  type="date"
                  name="dob"
                  margin="dense"
                  id="dob"
                  value={dob}
                  defaultValue="2021-05-24"
                  className="my-2 w-100"
                  error={errors.dobError}
                  helperText={errors.dobError}
                  onChange={checkAndValidate}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <ReCAPTCHA
                  sitekey="6Le0NvgaAAAAAA33a1nahKUi-ViSaXppdEky0uU8"
                  onChange={onCaptchaChange}
                  className="mt-3"
                  id="captcha"
                />
                <Button
                  variant="contained"
                  color="secondary"
                  className="w-50 mx-2 mt-2"
                  onClick={() => setStep(2)}
                  id="stepThree-prev"
                >
                  Previous
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className="w-25 mt-2"
                  id="register-submit"
                >
                  Submit
                </Button>
              </div>
            )}
          </form>
        </div>
      </div>
    </>
  );
}
