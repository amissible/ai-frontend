import React, { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-scroll";
import "./navbar.css";
import userContext from "../../userContext";
import { Link as RouterLink } from "react-router-dom";

export default function Navigationbar(props) {
  const { isLoggedIn, handleSignin } = useContext(userContext);
  return (
    <section className="navbar-section">
      <Navbar bg="" expand="lg" fixed="top" >
        <Navbar.Brand href="/" className="logo-text">
          Internsible
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="nav-items-list">
            {!isLoggedIn ? (
              <>
                {/* <Link
                  to="home"
                  spy={true}
                  smooth={true}
                  duration={1000}
                  className="link-text mx-3"
                >
                  Home
                </Link> */}

                <Link
                  to="about-us"
                  smooth={true}
                  offset={-75}
                  duration={1000}
                  className="link-text mx-3"
                >
                  About
                </Link>

                <Link
                  to="services"
                  spy={true}
                  smooth={true}
                  offset={-75}
                  duration={1000}
                  className="link-text mx-3"
                >
                  Services
                </Link>

                <Link
                  to="contact-us"
                  spy={true}
                  smooth={true}
                  offset={-75}
                  duration={1000}
                  className="link-text mx-3"
                >
                  Contact Us
                </Link>
                <Link
                  to="home"
                  spy={true}
                  smooth={true}
                  offset={-75}
                  duration={1000}
                  className="link-text"
                >
                  <RouterLink onClick={()=>handleSignin(true)} className="link-text mx-3">
                  Login
                </RouterLink>
                </Link>
                {/* <RouterLink onClick={()=>handleSignin(true)} className="link-text mx-3">
                  Login
                </RouterLink> */}
              </>
            ) : (
              <>
                <RouterLink to="/" className="link-text mx-3">
                  Home
                </RouterLink>
                <RouterLink to="/dashboard" className="link-text mx-3">
                  Dashboard
                </RouterLink>
                <Link to="/logout" className="link-text mx-3">
                  Logout
                </Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </section>
  );
}
