import React, { useState, useEffect, useContext } from "react";
import { Link } from 'react-router-dom';
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import "./signinform.css";
import { base_url } from "../../utils/helperData";
import axios from "axios";
import { useHistory } from "react-router";
import userContext from "../../userContext";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInForm() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState({});
  const classes = useStyles();
  const history = useHistory();
  const { userInfo, handleLoading, handleSnackbar, handleSignin, snackbarContentSetter } =
    useContext(userContext);

  const checkErrors = (value, rules) => {
    let isValid = true;
    if (rules.isRequired) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.length) {
      isValid = value.length !== 6 && isValid;
    }
    return isValid;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let newErrors;
    if (!checkErrors(email, { isRequired: true }))
      newErrors = { emailError: "Please enter a valid email" };
    else newErrors = undefined;

    if (!checkErrors(password, { isRequired: true }))
      newErrors = { passwordError: "Please enter a valid 6 digit OTP" };
    else newErrors = undefined;

    if (!newErrors) {
      handleLoading(true);
      setErrors({});
      localStorage.setItem("email", email);
      const payload = { email, password };
      try {
        const response = await axios.post(
          `${base_url}/quick_evaluation/verify`,
          payload
        );
        console.log(response);
        handleLoading(false);
        history.push("/dashboard");
      } catch (error) {
        handleLoading(false);
        snackbarContentSetter({
          severity: "error",
          title: "Failed!",
          text: "You have entered wrong OTP",
          subText: "Please try again!",
        });
        handleSnackbar(true);
      }
    } else {
      setErrors(newErrors);
    }
  };

  useEffect(() => {
    if(userInfo.email) setEmail(userInfo.email);
  },[userInfo.email]);

  return (
    <>
      <Container component="main" className="signinform-div pb-5" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign In
          </Typography>
          <form onSubmit={handleSubmit}>
            <TextField
              margin="normal"
              required
              fullWidth
              className="mb-4"
              id="login-email"
              label="Email Address"
              name="email"
              value={email}
              error={errors.emailError}
              onChange={e=>setEmail(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              className="my-3"
              name="password"
              label="Enter OTP"
              type="password"
              id="login-password"
              error={errors.passwordError}
              helperText={errors.password}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              id="login-submit"
            >
              Sign In
            </Button>
            <p>Not yet registered? <Link onClick={()=>handleSignin(false)}>Register</Link></p>
          </form>
        </div>
      </Container>
    </>
  );
}
