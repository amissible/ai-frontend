import React from "react";
import "./services.css";
import { Grid, Grow, Paper } from "@material-ui/core";
import AngularLogo from "../../assets/angular.png";
import ReactLogo from "../../assets/react.png"
import SpringbootLogo from "../../assets/springboot.png";
import JavaLogo from "../../assets/java.png";
import JiraLogo from "../../assets/jira.png";
import ConfluenceLogo from "../../assets/confluence.png";
import GitLogo from "../../assets/git-icon.png";
import BitbucketLogo from "../../assets/bitbucket.png";

export default function Services() {
  //Input 4 services in one array to have a good look
  const servicesArr1 = [
    { logo: AngularLogo, title: "Angular" },
    { logo: ReactLogo, title: "React" },
    { logo: SpringbootLogo, title: "Spring Boot" },
    { logo: GitLogo, title: "Git" },
  ];
  const servicesArr2 = [
    { logo: JavaLogo, title: "Java" },
    { logo: ConfluenceLogo, title: "Confluence" },
    { logo: JiraLogo, title: "Jira" },
    { logo: BitbucketLogo, title: "Bitbucket" },
  ];
  return (
    <div className="services-div pb-4" id="services">
      <div className="services-heading text-center mt-5 mb-5">
        <h1 className="text-white custom-heading">OUR SERVICES</h1>
        <h3 className="mt-3 mb-4">What We Provide?</h3>
      </div>
      <div className="services-container">
        <Grid container justify="center" className="my-5">
          <Grid item xs={12} md={10}>
            <Grow in={true} timeout={2000}>
              <Paper square elevation={12} className="p-4 text-justify">
                <h2 className="display-5 mb-4">Internsible Training:</h2>
                <p className="text-content">
                  Internsible offers a variety of online training programs
                  across multiple disciplines (Web Development, Java, Testing,
                  Automation, and many more) which students can do from their
                  homes and learn the skills needed in today’s industry.
                </p>
              </Paper>
            </Grow>
          </Grid>
        </Grid>

        <Grid container justify="space-evenly" className="my-5">
          {servicesArr1.map(({ logo, title }) => {
            return (
              <Grid item xs={6} md={2}>
                <div className="service-card">
                  <div className="card-img d-flex justify-content-center">
                    <img src={logo} alt="" />
                  </div>
                  <div className="card-title">
                    <h2 className="text-center py-2 display-6">{title}</h2>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>

        <Grid container justify="space-evenly" className="my-5">
        {servicesArr2.map(({ logo, title }) => {
            return (
              <Grid item xs={6} md={2}>
                <div className="service-card">
                  <div className="card-img d-flex justify-content-center">
                    <img src={logo} alt="" />
                  </div>
                  <div className="card-title">
                    <h2 className="text-center py-2 display-6">{title}</h2>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </div>
  );
}
